#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

# Run with openapi/swagger schema file as argument
yarn openapi-typescript $1 --output src/generated/dpSearchApiTypes.ts
