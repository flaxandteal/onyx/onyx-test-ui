FROM node:16 as chef
WORKDIR /app

# Install dependencies - this is the caching Docker layer
COPY package.json .
COPY yarn.lock .
RUN yarn install --production

FROM chef as builder
COPY . .
RUN yarn build --production

# We do not need the JS/TS toolchain to serve the compiled frontend
FROM bitnami/nginx:1.21
USER 0
RUN install_packages htop vim less
COPY --from=builder /app/build/ /app/
COPY nginx/* /opt/bitnami/nginx/conf/server_blocks/
USER 1000
ENV NGINX_HTTP_PORT_NUMBER=8000
EXPOSE 8000