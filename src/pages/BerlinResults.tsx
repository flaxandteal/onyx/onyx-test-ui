import { DecodedValueMap } from "use-query-params";
import { searchQP } from "../lib";
import { SearchResults } from "../generated/berlinSearchTypes";
import { stringify } from "query-string";
import axios from "axios";
import { useQuery } from "react-query";
import { Alert, LinearProgress, Stack, Typography } from "@mui/material";
import { OChip } from "../components/OChip";
import React from "react";

export const BerlinResults = (props: {
  qp: DecodedValueMap<typeof searchQP>;
}) => {
  const { qp } = props;
  const doFetchData = async (): Promise<SearchResults> => {
    let qs = stringify(qp) || "q=";
    const { data } = await axios.get<SearchResults>(`/berlin/search?${qs}`);
    return data;
  };
  const { isLoading, error, data } = useQuery(
    ["berlin", qp.q, qp.state, qp.limit, qp.ld],
    doFetchData
    // { enabled: true, cacheTime: 0 }
  );

  if (isLoading) {
    return <LinearProgress />;
  }
  if (error || !data) {
    return <Alert severity={"error"}>Failed to fetch search results</Alert>;
  }
  return (
    <Stack spacing={1}>
      {data.results ? (
        <>
          <Stack pb={2} spacing={1}>
            <Typography variant={"caption"}>
              Found <strong>{data.results.length} results</strong> in:{" "}
              <strong>{data.time}</strong>
            </Typography>
            <Stack direction="row" spacing={2}>
              <>
                <Typography>Normalized query:</Typography>
                <OChip l={data.query.normalized} />
              </>
              <>
                <Typography>Stop words:</Typography>
                {data.query.stop_words.map((s) => (
                  <OChip l={s} key={s} />
                ))}
              </>
              <>
                <Typography>Codes:</Typography>
                {data.query.codes.map((c) => (
                  <OChip l={c} key={c} />
                ))}
              </>
              <>
                <Typography>Exact Matches:</Typography>
                {data.query.exact_matches.map((e) => (
                  <OChip l={e} key={e} />
                ))}
              </>
            </Stack>
            <Stack direction={"row"}>
              <Typography>Not exact matches:</Typography>
              {data.query.not_exact_matches.map((n) => (
                <OChip l={n} key={n} />
              ))}
            </Stack>
          </Stack>
          {data.results.map((res) => (
            <Stack
              key={res.loc.id}
              borderRadius={1}
              border={"1px solid lightgrey"}
              pl={1}
              p={1}
            >
              <Stack direction={"row"} spacing={2}>
                <Typography fontWeight={"bold"} fontSize={"large"}>
                  {res.loc.names[0]}
                </Typography>
                <Typography>{res.loc.encoding}</Typography>
                <Typography>{res.loc.id}</Typography>
                <Typography>Score: {res.score}</Typography>
              </Stack>
              <Stack direction={"row"} spacing={2}>
                <Typography>Codes: {res.loc.codes.join(", ")}</Typography>
                <Typography>
                  State: {res.loc.state[0]} - {res.loc.state[1]}
                </Typography>
                {res.loc.subdiv ? (
                  <Typography>
                    Subdivision: {res.loc.subdiv[0]} - {res.loc.subdiv[1]}
                  </Typography>
                ) : null}
              </Stack>
            </Stack>
          ))}
        </>
      ) : null}
    </Stack>
  );
};
