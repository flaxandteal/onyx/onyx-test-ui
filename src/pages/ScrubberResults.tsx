import { DecodedValueMap } from "use-query-params";
import { searchQP } from "../lib";
import { stringify } from "query-string";
import axios from "axios";
import { useQuery } from "react-query";
import { Alert, Box, LinearProgress, Stack, Typography } from "@mui/material";
import React from "react";
import { ScrubberResp } from "../generated/scrubberSearchTypes";

export const ScrubberResults = (props: {
  qp: DecodedValueMap<typeof searchQP>;
}) => {
  const qp = props.qp;
  const doFetchData = async (): Promise<ScrubberResp> => {
    const query = { q: qp.q, limit: qp.limit };
    let qs = stringify(query);
    const { data } = await axios.get<ScrubberResp>(`/scrubber/search?${qs}`);
    return data;
  };
  const { isLoading, error, data } = useQuery(
    ["scrubber", qp.q, qp.limit],
    doFetchData
  );
  if (error) {
    return <Alert severity={"error"}>Failed to fetch search results</Alert>;
  }
  return (
    <Stack spacing={2}>
      {isLoading ? <LinearProgress /> : null}
      {data?.results ? (
        <>
          <Typography variant={"caption"}>
            Found {data.results.industries.length} industries and{" "}
            {data.results.areas.length} areas in {data.time}
          </Typography>
          <Stack direction={"row"} spacing={4}>
            <Stack spacing={1}>
              <Typography variant={"h6"}>Industries</Typography>
              {data.results.industries.map((industr) => (
                <Box key={industr.code}>
                  <Typography>
                    {industr.code} {industr.name}
                  </Typography>
                </Box>
              ))}
            </Stack>
            <Stack direction={"column"} spacing={1}>
              <Typography variant={"h6"}>Areas</Typography>
              {data.results.areas.map((area) => {
                const more = area.codes.length > 10 ? " ...more" : "";
                return (
                  <Box key={area.name} borderRadius={2} border={1} p={0.5}>
                    <Typography>
                      <strong>{area.name}</strong>, {area.region}
                    </Typography>
                    <Typography>
                      {area.codes.length} Codes:{" "}
                      {area.codes.slice(0, 11).join(", ")}
                      {more}
                    </Typography>
                  </Box>
                );
              })}
            </Stack>
          </Stack>
        </>
      ) : null}
    </Stack>
  );
};
