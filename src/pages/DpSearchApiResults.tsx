import { definitions, paths } from "../generated/dpSearchApiTypes";
import { DecodedValueMap, useQueryParams } from "use-query-params";
import { searchQP } from "../lib";
import { stringify } from "query-string";
import axios from "axios";
import { useQuery } from "react-query";
import {
  Alert,
  Box,
  FormControlLabel,
  LinearProgress,
  Stack,
  Switch,
  Typography,
} from "@mui/material";
import { OChip } from "../components/OChip";
import React from "react";
import { default as htmlParse } from "html-react-parser";

type DpQueryType = paths["/search"]["get"]["parameters"]["query"];
type DpSearchResults = definitions["GetSearchResponse"];
export const DpSearchApiResults = (props: {
  qp: DecodedValueMap<typeof searchQP>;
}) => {
  const [qp, setQp] = useQueryParams(searchQP);
  const doFetchData = async (): Promise<DpSearchResults> => {
    const query: DpQueryType & { c: 0 | 1 } = {
      q: props.qp.q,
      limit: props.qp.limit,
      c: qp.useNlpHub ? 1 : 0,
    };
    const uri = `/dp-search-api/search?${stringify(query)}`;
    const { data } = await axios.get<DpSearchResults>(uri);
    return data;
  };
  const { isLoading, error, data } = useQuery(
    ["dp-search-api", qp.q, qp.limit, qp.useNlpHub],
    doFetchData
  );
  if (error) {
    return <Alert severity={"error"}>Failed to fetch search results</Alert>;
  }
  return (
    <Stack spacing={2}>
      {isLoading ? <LinearProgress /> : null}
      <FormControlLabel
        control={
          <Switch
            checked={qp.useNlpHub}
            onChange={(c, checked) => setQp({ useNlpHub: checked })}
          />
        }
        label={`Flax&Teal code is ${qp.useNlpHub ? "on" : "off"}`}
      />
      {data?.items ? (
        <>
          <Typography variant={"caption"}>
            Found {data.count} results in {data.took}ms
          </Typography>
          <Stack direction={"row"} spacing={4}>
            <Typography>
              Suggestions: {data.suggestions?.join(", ")}{" "}
            </Typography>
            <Typography>
              Alternative Suggestions:{" "}
              {data.alternative_suggestions?.join(", ")}{" "}
            </Typography>
          </Stack>
          <Box>
            By content type:
            {data.content_types.map((ct) => (
              <OChip key={ct.type} l={`${ct.type}: ${ct.count}`} />
            ))}
          </Box>
          <Stack direction={"column"} spacing={1}>
            {data.items.map((item) => (
              <Box key={item.uri} borderRadius={2} border={1} p={0.5}>
                <Typography>
                  <a
                    href={`https://www.ons.gov.uk${item.uri}`}
                    target="_blank"
                    rel="noopener noreferrer nofollow"
                  >
                    {item.description.title ||
                      item.description.headline1 ||
                      item.description.headline2 ||
                      item.description.headline3 ||
                      "link"}
                  </a>
                </Typography>
                <Typography>
                  Type: {item.type} date: {item.description.release_date}
                </Typography>
                <Box>
                  <strong>Summary: </strong>
                  {htmlParse(item.description.highlight?.summary || "")}
                </Box>
              </Box>
            ))}
          </Stack>
        </>
      ) : null}
    </Stack>
  );
};
