import { DecodedValueMap } from "use-query-params";
import { searchQP } from "../lib";
import { stringify } from "query-string";
import axios from "axios";
import { useQuery } from "react-query";
import { Alert, LinearProgress, Stack, Typography } from "@mui/material";
import React from "react";
import * as t from "io-ts";
import { pipe } from "fp-ts/pipeable";
import { fold } from "fp-ts/Either";
import { OChip } from "../components/OChip";

const FFResult = t.type({ s: t.number, c: t.array(t.string) });
const FFResults = t.array(FFResult);

export const FfFasttextApiResults = (props: {
  qp: DecodedValueMap<typeof searchQP>;
}) => {
  const qp = props.qp;
  const doFetchData = async (): Promise<t.TypeOf<typeof FFResults>> => {
    const query = { query: qp.q };
    let qs = stringify(query);
    // /ff-fasttext-api/categories?query=mortality
    const { data } = await axios.get<any>(`/ff-fasttext-api/categories?${qs}`);
    return pipe(
      FFResults.decode(data),
      fold(
        (_e: t.Errors) => {
          alert("cannot decode ff-fasttext-api response");
          throw new Error("Cannot decode ff-fasttext-api response");
        },
        (r) => r
      )
    );
  };
  const { isLoading, error, data } = useQuery(
    ["ff-fasttext-api", qp.q],
    doFetchData
  );
  if (error) {
    return <Alert severity={"error"}>Failed to fetch search results</Alert>;
  }
  return (
    <Stack spacing={2}>
      {isLoading ? <LinearProgress /> : null}
      {data ? (
        <>
          <Stack spacing={1}>
            <Typography variant={"h6"}>
              Results from the category server
            </Typography>
            {data.map((r) => (
              <Stack
                key={r.c.join("-")}
                direction={"row"}
                borderBottom={1}
                spacing={1}
                p={1}
              >
                <Typography>
                  score: <strong>{r.s.toFixed(3)}</strong>
                </Typography>
                {r.c.map((c) => (
                  <OChip key={c} l={c} />
                ))}
              </Stack>
            ))}
          </Stack>
        </>
      ) : null}
    </Stack>
  );
};
