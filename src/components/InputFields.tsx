import { TextField, TextFieldProps } from "@mui/material";
import React, { useState } from "react";

export const StringInputField = (
  props: {
    value: string;
    handler: (value: string) => void;
    maxLength?: number;
  } & TextFieldProps
) => {
  const [state, setState] = useState(props.value);
  return (
    <TextField
      fullWidth={props.fullWidth}
      label={props.label}
      autoFocus={props.autoFocus}
      inputProps={{ maxLength: props.maxLength }}
      value={state}
      onChange={(e) => {
        setState(e.target.value);
      }}
      onKeyPress={(e) => {
        if (e.key === "Enter") {
          props.handler(state);
        }
      }}
      onBlur={() => props.handler(state)}
    />
  );
};

export const NumberInputField = (
  props: {
    value: string;
    handler: (value: number) => void;
  } & TextFieldProps
) => {
  const [state, setState] = useState(props.value);
  const doSubmit = () => {
    const limit = parseInt(state, 10) || 1;
    props.handler(limit);
  };
  return (
    <TextField
      value={state}
      label={props.label}
      type={"number"}
      inputProps={{ inputMode: "numeric", pattern: "[0-9]*" }}
      onChange={(e) => setState(e.target.value)}
      onKeyPress={(e) => {
        if (e.key === "Enter") {
          doSubmit();
        }
      }}
      onBlur={doSubmit}
    />
  );
};
