import { searchQP, TSearchBackend } from "../lib";
import { useQueryParams } from "use-query-params";
import { Button } from "@mui/material";
import React from "react";

export const SearchBackendSelectorButton = (props: {
  backend: TSearchBackend;
}) => {
  const [qp, setQp] = useQueryParams(searchQP);
  const bk = qp.searchBackend;
  return (
    <Button
      variant={bk == props.backend ? "contained" : "outlined"}
      onClick={() => setQp({ searchBackend: props.backend })}
    >
      {props.backend}
    </Button>
  );
};
