import { Chip, ChipProps } from "@mui/material";
import React from "react";

export const OChip = (props: { l: string } & ChipProps) => (
  <Chip
    style={{ marginRight: 2 }}
    variant={"outlined"}
    size={"small"}
    label={props.l}
  />
);
