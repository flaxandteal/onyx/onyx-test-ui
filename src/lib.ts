import {
  BooleanParam,
  NumberParam,
  StringParam,
  withDefault,
} from "use-query-params";

export const SearchBackends = [
  "dp-search-api",
  "scrubber",
  "berlin",
  "ff-fasttext-api",
] as const;
export type TSearchBackend = typeof SearchBackends[number];

export const searchQP = {
  q: withDefault(StringParam, ""),
  useNlpHub: withDefault(BooleanParam, true),
  limit: withDefault(NumberParam, 10),
  ld: withDefault(NumberParam, 2),
  state: StringParam,
  searchBackend: withDefault(StringParam, "dp-search-api"),
};
