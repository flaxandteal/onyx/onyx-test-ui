import React from "react";
import {
  Alert,
  Button,
  ButtonGroup,
  Card,
  Container,
  Stack,
  Typography,
} from "@mui/material";
import { DecodedValueMap, useQueryParams } from "use-query-params";
import { SearchBackends, searchQP, TSearchBackend } from "./lib";
import { NumberInputField, StringInputField } from "./components/InputFields";
import { SearchBackendSelectorButton } from "./components/SearchBackendSelectorButton";
import { BerlinResults } from "./pages/BerlinResults";
import { DpSearchApiResults } from "./pages/DpSearchApiResults";
import { ScrubberResults } from "./pages/ScrubberResults";
import { FfFasttextApiResults } from "./pages/FfFasttextApiResults";

export const SearchPage = () => {
  const [qp, setQp] = useQueryParams(searchQP);
  return (
    <Stack spacing={1} pt={1}>
      <Card style={{ padding: 8, marginTop: 4 }}>
        <Stack spacing={1}>
          <Stack direction={"row"} spacing={1}>
            <StringInputField
              label={"Search query"}
              fullWidth
              autoFocus
              value={qp.q || ""}
              handler={(q) => setQp({ q })}
            />
            <NumberInputField
              value={qp.limit.toString()}
              label={"Limit"}
              handler={(limit) => setQp({ limit })}
            />
            {qp.searchBackend === "berlin" && (
              <>
                <StringInputField
                  value={qp.state || ""}
                  handler={(state) => setQp({ state: state || undefined })}
                  label={"Filter by country code"}
                  maxLength={2}
                />
                <NumberInputField
                  label={"Levenshtein distance"}
                  value={qp.ld.toString()}
                  handler={(ld) => setQp({ ld })}
                />
              </>
            )}
            <Button variant={"outlined"} onClick={() => setQp({})}>
              Search
            </Button>
          </Stack>
          <Stack direction={"row"} spacing={0.5}>
            <Typography variant={"overline"}>Search Backend: </Typography>
            <ButtonGroup variant={"outlined"} disableElevation>
              {SearchBackends.map((b) => (
                <SearchBackendSelectorButton key={b} backend={b} />
              ))}
            </ButtonGroup>
          </Stack>
        </Stack>
      </Card>
      <Results qp={qp} />
    </Stack>
  );
};
const Results = (props: { qp: DecodedValueMap<typeof searchQP> }) => {
  const backend: TSearchBackend = props.qp.searchBackend as TSearchBackend;
  switch (backend) {
    case "scrubber":
      return <ScrubberResults {...props} />;
    case "berlin":
      return <BerlinResults {...props} />;
    case "dp-search-api":
      return <DpSearchApiResults {...props} />;
    case "ff-fasttext-api":
      return <FfFasttextApiResults qp={props.qp} />;
    default:
      return <Alert severity={"error"}>{backend} is not implemented</Alert>;
  }
};

function App() {
  return (
    <Container maxWidth={"xl"}>
      <SearchPage />
    </Container>
  );
}

export default App;
