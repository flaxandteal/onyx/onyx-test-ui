/* tslint:disable */
/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */

export interface ScrubberResp {
  query: string;
  results: Results;
  time: string;
  [k: string]: unknown;
}
export interface Results {
  areas: AreaResp[];
  industries: IndustryResp[];
  [k: string]: unknown;
}
export interface AreaResp {
  codes: string[];
  name: string;
  region: string;
  region_code: string;
  [k: string]: unknown;
}
export interface IndustryResp {
  code: string;
  name: string;
  [k: string]: unknown;
}
