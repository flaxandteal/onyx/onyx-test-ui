#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

filename="scrubber-search-schema.json"

wget http://localhost:3002/scrubber/json-schema -O $filename
yarn json2ts -i $filename -o src/generated/scrubberSearchTypes.ts
rm $filename
