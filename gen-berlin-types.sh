#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

filename="berlin-search-schema.json"

wget http://localhost:3001/berlin/search-schema -O $filename
yarn json2ts -i $filename -o src/generated/berlinSearchTypes.ts
rm $filename
